use sqlx::sqlite::{SqlitePool, SqlitePoolOptions};
use std::sync::{Arc, RwLock};
use teloxide::prelude::*;
use teloxide::utils::command::{BotCommands, ParseError};

pub type Bot = teloxide::adaptors::DefaultParseMode<teloxide::Bot>;

pub mod model;
pub mod tracking;
pub mod tracking_loop;

fn tracking_parser(args: String) -> Result<(String, Option<String>), ParseError> {
    if args.is_empty() {
        return Err(ParseError::TooFewArguments {
            expected: 1,
            found: 0,
            message: "Must send at least a tracking number.".to_string(),
        });
    }

    let mut split = args.split(' ');
    let id = split.next().unwrap().to_string();
    let description = split.collect::<Vec<&str>>().join(" ");

    Ok((
        id,
        if description.is_empty() {
            None
        } else {
            Some(description)
        },
    ))
}

#[derive(BotCommands, Clone)]
#[command(
    rename_rule = "lowercase",
    description = "These commands are supported:"
)]
enum Command {
    #[command(description = "Display this text")]
    Help,
    #[command(description = "Add tracking number to the tracker", parse_with = tracking_parser)]
    Track {
        id: String,
        description: Option<String>,
    },
    #[command(description = "Remove tracking number from the tracker")]
    Remove { id: String },
    #[command(description = "List all tracking numbers status")]
    List,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    dotenv::dotenv().ok();
    pretty_env_logger::init();

    log::info!("Starting database pool...");

    let pool = SqlitePoolOptions::new()
        .max_connections(5)
        .connect(dotenv_codegen::dotenv!("DATABASE_URL"))
        .await?;

    let now = Arc::new(RwLock::new(time::OffsetDateTime::UNIX_EPOCH));

    log::info!("Starting bot...");

    let bot = teloxide::Bot::from_env().parse_mode(teloxide::types::ParseMode::MarkdownV2);
    bot.set_my_commands(Command::bot_commands()).await?;

    let handler = Update::filter_message()
        .branch(dptree::entry().filter_command::<Command>().endpoint(answer))
        .branch(
            dptree::filter(|msg: Message| {
                msg.text().is_some() && msg.text().unwrap().starts_with('/')
            })
            .endpoint(|msg: Message, bot: Bot| async move {
                bot.send_message(msg.chat.id, "Invalid command").await?;
                Ok(())
            }),
        );

    tracking_loop::start_loop(bot.clone(), pool.clone(), now.clone()).await;

    Dispatcher::builder(bot, handler)
        .dependencies(dptree::deps![pool, now.clone()])
        .error_handler(LoggingErrorHandler::with_custom_text(
            "An error has occurred in the dispatcher",
        ))
        .enable_ctrlc_handler()
        .build()
        .dispatch()
        .await;

    Ok(())
}

async fn answer(
    bot: Bot,
    msg: Message,
    cmd: Command,
    pool: SqlitePool,
    now: Arc<RwLock<time::OffsetDateTime>>,
) -> ResponseResult<()> {
    match cmd {
        Command::Help => help(bot, msg).await,
        Command::Track { id, description } => {
            tracking::track(bot, msg, pool, id, description).await
        }
        Command::Remove { id } => tracking::remove(bot, msg, pool, id).await,
        Command::List => tracking::list(bot, msg, pool, now).await,
    }
}

async fn help(bot: Bot, msg: Message) -> ResponseResult<()> {
    bot.send_message(msg.chat.id, Command::descriptions().to_string())
        .await?;

    Ok(())
}
