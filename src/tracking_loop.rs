use crate::model::Tracking;
use reqwest::header::{HeaderMap, ACCEPT};
use reqwest::Client;
use sqlx::SqlitePool;
use std::sync::{Arc, RwLock};
use std::time::Duration;
use teloxide::requests::Requester;
use teloxide::types::ChatId;
use teloxide::utils::markdown;

struct DatabaseTrackingData {
    //tracking_number: String,
    //carrier: String,
    description: String,
    date: String,
    status: String,
}

async fn loop_content(
    bot: &crate::Bot,
    client: &Client,
    pool: SqlitePool,
    now: Arc<RwLock<time::OffsetDateTime>>,
    group_id: i64,
) {
    let ids = {
        let query = sqlx::query!("SELECT * FROM tracking WHERE carrier = 'default'")
            .fetch_all(&pool)
            .await;

        if let Err(why) = query {
            log::error!("Error running database query: {}", why);
            return;
        }

        let main_query_data = query.unwrap();

        let ids = main_query_data
            .iter()
            .map(|i| i.tracking_number.as_str())
            .collect::<Vec<_>>()
            .join(",");

        if ids.is_empty() {
            log::debug!("No tracking numbers to check; skipping...");
            return;
        } else {
            ids
        }
    };

    let url = format!("http://10.0.0.110:8080/track/{ids}");
    //let url = format!("http://10.0.0.110:8080/track/PH8GH60723802150108620E");

    log::debug!("{}", url);

    let raw_data = client
        .get(url)
        .send()
        .await;

    if let Err(why) = raw_data {
        log::error!("Error making HTTP request: {}", why);
        return;
    }

    let data = raw_data.unwrap();

    log::debug!("{}", data.status());

    //let raw_text = data.text().await.unwrap();
    //log::debug!("gay -> {}", raw_text);

    let raw_json = data.json::<Tracking>().await;

    if let Err(why) = raw_json {
        log::error!("Error parsing JSON: {}", why);
        return;
    }

    let data = raw_json.unwrap();

    log::debug!("{:#?}", data);

    *now.write().unwrap() = time::OffsetDateTime::now_utc();

    for tracking in data {
        for (idx, i) in tracking.data.iter().enumerate() {
            log::debug!("{} -> {:#?}", idx, i);

            if i.last_event.is_empty() || i.last_update_time.is_empty() {
                continue;
            }

            let query = sqlx::query!(
                "SELECT * FROM tracking WHERE tracking_number = $1 AND carrier = $2",
                i.tracking_number,
                i.carrier_code
            )
            .fetch_optional(&pool)
            .await;

            if let Err(why) = query {
                log::error!("Error running database query: {}", why);
                return;
            }

            let main_query_data = query.unwrap();

            let database_data = if let Some(d) = main_query_data {
                DatabaseTrackingData {
                    //tracking_number: d.tracking_number,
                    //carrier: d.carrier,
                    description: d.description,
                    date: d.date,
                    status: d.status,
                }
            } else {
                let query = sqlx::query!("INSERT INTO tracking (tracking_number, carrier, description) VALUES ($1, $2, (SELECT description FROM tracking WHERE tracking_number = $1 AND carrier = 'default')) RETURNING *",
                    i.tracking_number,
                    i.carrier_code)
                    .fetch_optional(&pool)
                    .await;

                if let Err(why) = query {
                    log::error!("Error running database query: {}", why);
                    return;
                }

                let d = query.unwrap().unwrap();

                DatabaseTrackingData {
                    //tracking_number: d.tracking_number.unwrap(),
                    //carrier: d.carrier.unwrap(),
                    description: d.description.unwrap(),
                    date: d.date,
                    status: d.status,
                }
            };

            if database_data.status != i.last_event || database_data.date != i.last_update_time {
                let query = sqlx::query!(
                    "UPDATE tracking SET date = $3, status = $4 WHERE tracking_number = $1 AND carrier = $2; UPDATE tracking SET date = $3, status = $4 WHERE tracking_number = $1 AND carrier = 'default'",
                    i.tracking_number,
                    i.carrier_code,
                    i.last_update_time,
                    i.last_event,
                    i.tracking_number,
                    i.last_update_time,
                    i.last_event,
                )
                .execute(&pool)
                .await;

                let res = bot
                    .send_message(
                        ChatId(group_id),
                        format!(
                            "Update on package {} \\- {}\\!\n\n{}\n{} \\-\\> {}",
                            markdown::bold(&markdown::escape(&i.tracking_number)),
                            markdown::bold(&markdown::escape(&i.carrier_code)),
                            markdown::escape(&database_data.description),
                            markdown::escape(&i.last_update_time),
                            markdown::escape(&i.last_event),
                        ),
                    )
                    .await;

                if let Err(why) = res {
                    log::error!("Error sending message: {}", why);
                    return;
                }

                if let Err(why) = query {
                    log::error!("Error running database query: {}", why);
                    return;
                }
            }
        }

        //if value.is_empty() {
        //    continue;
        //}

        //let record = main_query_data
        //    .iter()
        //    .find(|i| i.tracking_number == key)
        //    .unwrap();

        //if &record.status != value.last().unwrap().last().unwrap() {
        //}
    }
}

pub async fn start_loop(bot: crate::Bot, pool: SqlitePool, now: Arc<RwLock<time::OffsetDateTime>>) {
    let group_id = std::env::var("GROUP_ID").unwrap().parse().unwrap();
    let iter_time = std::env::var("ITER_TIME").unwrap().parse().unwrap();

    let client = reqwest::ClientBuilder::new()
        .default_headers({
            let mut map = HeaderMap::new();
            map.insert(ACCEPT, "application/json".parse().unwrap());
            map
        })
        .build()
        .unwrap();

    tokio::spawn(async move {
        tokio::time::sleep(Duration::from_secs(5)).await;

        loop {
            log::debug!("Running...");
            loop_content(&bot, &client, pool.clone(), now.clone(), group_id).await;

            log::debug!("Waiting...");
            tokio::time::sleep(Duration::from_secs(iter_time)).await;
        }
    });
}
