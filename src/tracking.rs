use crate::Bot;
use reqwest::header::{HeaderMap, ACCEPT};
use sqlx::SqlitePool;
use std::sync::{Arc, RwLock};
use teloxide::prelude::*;
use teloxide::utils::markdown;
use time::format_description::well_known::Rfc2822;

pub async fn track(
    bot: Bot,
    msg: Message,
    pool: SqlitePool,
    id: String,
    description: Option<String>,
) -> ResponseResult<()> {
    let description = description.unwrap_or("No Description".to_string());

    let client = reqwest::ClientBuilder::new()
        .default_headers({
            let mut map = HeaderMap::new();
            map.insert(ACCEPT, "application/json".parse().unwrap());
            map
        })
        .build()
        .unwrap();

    let req = client
        .get(&format!("http://10.0.0.110:8080/track/{id}"))
        .send()
        .await;

    if let Err(why) = req {
        bot.send_message(
            msg.chat.id,
            format!(
                "Error sending GET request: {}",
                markdown::escape(&why.to_string())
            ),
        )
        .await?;

        return Ok(());
    }

    let query = sqlx::query!(
        "INSERT INTO tracking (tracking_number, description, carrier) VALUES ($1, $2, 'default')",
        id,
        description,
    )
    .execute(&pool)
    .await;

    if let Err(why) = query {
        bot.send_message(
            msg.chat.id,
            format!(
                "Error inserting value: {}",
                markdown::escape(&why.to_string())
            ),
        )
        .await?;

        return Ok(());
    }

    let text = format!(
        "Added to tracking:\n{} \\| {}",
        markdown::bold(&markdown::escape(&id)),
        markdown::escape(&description)
    );

    bot.send_message(msg.chat.id, text).await?;

    Ok(())
}

pub async fn remove(bot: Bot, msg: Message, pool: SqlitePool, id: String) -> ResponseResult<()> {
    let query = sqlx::query!(
        "DELETE FROM tracking WHERE tracking_number = $1 RETURNING *",
        id
    )
    .fetch_optional(&pool)
    .await;

    if let Err(why) = query {
        bot.send_message(
            msg.chat.id,
            format!(
                "Error removing value: {}",
                markdown::escape(&why.to_string())
            ),
        )
        .await?;

        return Ok(());
    }

    let client = reqwest::ClientBuilder::new()
        .default_headers({
            let mut map = HeaderMap::new();
            map.insert(ACCEPT, "application/json".parse().unwrap());
            map
        })
        .build()
        .unwrap();

    let req = client
        .delete(&format!("http://10.0.0.110:8080/track/{id}"))
        .send()
        .await;

    if let Err(why) = req {
        bot.send_message(
            msg.chat.id,
            format!(
                "Error sending DELETE request: {}",
                markdown::escape(&why.to_string())
            ),
        )
        .await?;

        return Ok(());
    }

    let text = if let Some(i) = query.unwrap() {
        format!(
            "Removed:\n{} \\| {}\n{} \\-\\> {}\n\n",
            markdown::bold(&markdown::escape(&i.tracking_number.unwrap())),
            markdown::escape(&i.description.unwrap()),
            markdown::escape(&i.date.unwrap()),
            markdown::escape(&i.status.unwrap())
        )
    } else {
        format!(
            "Tracking number {} not found\\.",
            markdown::bold(&markdown::escape(&id))
        )
    };

    bot.send_message(msg.chat.id, text).await?;

    Ok(())
}

pub async fn list(
    bot: Bot,
    msg: Message,
    pool: SqlitePool,
    now: Arc<RwLock<time::OffsetDateTime>>,
) -> ResponseResult<()> {
    let query = sqlx::query!("SELECT * FROM tracking WHERE carrier = 'default'")
        .fetch_all(&pool)
        .await;

    if let Err(why) = query {
        bot.send_message(
            msg.chat.id,
            format!(
                "Error inserting value: {}",
                markdown::escape(&why.to_string())
            ),
        )
        .await?;

        return Ok(());
    }

    let text = query
        .unwrap()
        .iter()
        .map(|i| {
            format!(
                "{}\n{}\n{} \\-\\> {}\n\n",
                markdown::bold(&markdown::escape(&format!("={}=", i.tracking_number))),
                markdown::code_inline(&i.description),
                markdown::escape(&i.date),
                markdown::escape(&i.status)
            )
        })
        .collect::<String>();

    bot.send_message(
        msg.chat.id,
        format!(
            "Currently tracking:\n\n{text}\nLast checked: {} UTC",
            markdown::escape(&now.read().unwrap().format(&Rfc2822).unwrap())
        ),
    )
    .await?;

    Ok(())
}
