use serde::{Deserialize, Serialize};

pub type Tracking = Vec<TrackingElement>;

#[derive(Debug, Serialize, Deserialize)]
pub struct TrackingElement {
    pub data: Vec<Datum>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Datum {
    pub tracking_number: String,
    pub carrier_code: String,
    #[serde(default)]
    #[serde(rename = "lastEvent")]
    pub last_event: String,
    #[serde(default)]
    #[serde(rename = "lastUpdateTime")]
    pub last_update_time: String,
}
