-- Add up migration script here
ALTER TABLE tracking RENAME TO tracking_old;

CREATE TABLE tracking (
    tracking_number TEXT NOT NULL,
    carrier TEXT NOT NULL,

    description TEXT NOT NULL DEFAULT 'No Description',

    date TEXT NOT NULL DEFAULT '1970-01-01 00:00:00.000',
    status TEXT NOT NULL DEFAULT 'No Status',

    PRIMARY KEY(tracking_number, carrier)
);
