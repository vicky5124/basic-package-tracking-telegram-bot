-- Add up migration script here
CREATE TABLE tracking (
    tracking_number TEXT NOT NULL PRIMARY KEY,
    description TEXT NOT NULL DEFAULT 'No Description',

    date TEXT NOT NULL DEFAULT '1970-01-01 00:00:00.000',
    status TEXT NOT NULL DEFAULT 'No Status'
);
