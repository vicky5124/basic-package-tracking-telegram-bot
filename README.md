# Basic Package Tracking Telegram Bot

The `.env` file:

```bash
TELOXIDE_TOKEN="telegram_token"
RUST_LOG="info,sqlx=warn"
DATABASE_URL="sqlite://tracking.db"
GROUP_ID="-815770501"
ITER_TIME="3600"
```

`cargo sqlx migrate run`

`cargo run`
